package Model

import java.util.UUID

case class Entity(
                   siloId: UUID,
                   entityId: String,
                 )
