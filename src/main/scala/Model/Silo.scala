package Model

import java.util.UUID

case class Silo(
                 siloId: UUID,
                 name: Map[String, String]
               )
