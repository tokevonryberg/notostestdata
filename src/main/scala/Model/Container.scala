package Model

import java.util.UUID

case class Container(
                      siloId: UUID,
                      entityId: String,
                      containerId: UUID,
                      x1: Map[String, String]
                    )

case class ContainerAssociation(
                                 siloId: UUID,
                                 fromEntityId: String,
                                 fromContainerId: UUID,
                                 associationTypeId: String = "",
                                 toEntityId: String,
                                 toContainerId: UUID,
                                 equalFromTo: Boolean = false
                               )


