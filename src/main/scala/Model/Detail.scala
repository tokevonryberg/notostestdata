package Model

import java.util.UUID

import org.joda.time.DateTime

case class Detail(
                  siloId: UUID,
                  entityId: String,
                  detailId: String,
                  controlType: String,
                  dataType: String,
                  detailType: Set[String]=Set("value"),
                  listOfValues: Map[String, Map[String, String]]=null
                 )


