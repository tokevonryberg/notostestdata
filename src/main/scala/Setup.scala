import org.apache.spark.{SparkConf, SparkContext}

object Setup {
  val conf = new SparkConf(true)
    .set("spark.cassandra.connection.host", "127.0.0.1")
    .set("spark.cassandra.auth.username", "cassandra")
    .set("spark.cassandra.auth.password", "cassandra")

  def connect(): SparkContext = {
    new SparkContext(conf)
  }

  def disconnect(sc: SparkContext): Unit = {
    sc.stop()
  }
}
