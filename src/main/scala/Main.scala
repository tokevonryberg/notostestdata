import java.util.{Calendar, GregorianCalendar, Random, UUID}
import com.datastax.spark.connector.toRDDFunctions
import Model._
import Setup._
import com.datastax.spark.connector.SomeColumns
import com.datastax.driver.core.utils.UUIDs
import com.datastax.spark.connector.cql.CassandraConnector
import org.apache.spark.SparkConf
import scala.collection.immutable.IndexedSeq
import scala.collection.mutable.ListBuffer
import scala.util.Try

object Main {
  def main(args: Array[String]) {
    if (args.length != 1)
      throw new IllegalArgumentException("Expected one arguments. Got: " +
        args.length)

    val numberOfPatients = Try(Integer.parseInt(args(0))).getOrElse(throw new
        IllegalArgumentException("Argument numberOfPatients must be an " +
          "integer. Got: " + args(0)))
    lazy val sc = connect()

    println("Truncating keyspace")
    truncateKeyspace(sc.getConf)

    println("Inserting silos")
    val silo = Silo(UUIDs.timeBased(), Map("en-us" -> "Notos test silo"))
    sc.parallelize(Seq(silo))
      .saveToCassandra("notos_silo", "silo", SomeColumns("silo_id",
        "disabled", "name"))

    println("Inserting entities")
    val patient = Entity(silo.siloId, "patient")
    val background = Entity(silo.siloId, "background")
    val generalHealth = Entity(silo.siloId, "general_health_symptoms")
    val physicalActivities = Entity(silo.siloId, "physical_activities")
    val activities = Entity(silo.siloId, "daily_activities")
    val medicalCare = Entity(silo.siloId, "medical_care")
    val entities = Seq(patient, background, generalHealth,
      physicalActivities, activities, medicalCare)
    sc.parallelize(entities)
      .saveToCassandra("notos_metadata", "entity", SomeColumns("silo_id",
        "entity_id", "second_type_in"))

    println("Inserting details")
    val details = new ListBuffer[Detail]
    details += Detail(silo.siloId, patient.entityId, "gender", "radiobutton",
      "string", listOfValues = Map("Male" -> Map("en-us" -> "Male"), "Female"
        -> Map("en-us" -> "Female"), "Unknown" -> Map("en-us" -> "Unknown")))
    details += Detail(silo.siloId, patient.entityId, "birthday",
      "text-field", "date")

    details += Detail(silo.siloId, background.entityId, "ethnic_origin",
      "radiobuttion", "string", listOfValues = Map("White" -> Map("en-us" ->
        "White not Hispanic"), "Black" -> Map("en-us" -> "Black not " +
        "Hispanic"), "Hispanic" -> Map("en-us" -> "Hispanic"), "Asian" -> Map
      ("en-us" -> "Asian or Pacific Islander"), "Filipino" -> Map("en-us" ->
        "Filipino"), "Indian" -> Map("en-us" -> "American Indian/Alaskan " +
        "Native"), "Other" -> Map("en-us" -> "Other")))
    details += Detail(silo.siloId, background.entityId, "education",
      "radiobutton", "string", listOfValues = Map("Primary" -> Map("en-us" ->
        "Primary"), "High school" -> Map("en-us" -> "High school"),
        "College/university" -> Map("en-us" -> "College/university"),
        "Graduate school" -> Map("en-us" -> "Graduate school")))
    details += Detail(silo.siloId, background.entityId, "civil_status",
      "radiobutton", "string", listOfValues = Map("Married" -> Map("en-us" ->
        "Married"), "Separated" -> Map("en-us" -> "Separated"), "Widowed" ->
        Map("en-us" -> "Widowed"), "Single" -> Map("en-us" -> "Single"),
        "Divorced" -> Map("en-us" -> "Divorced")))
    details += Detail(silo.siloId, background.entityId, "diabetes",
      "radiobuttion", "string", listOfValues = Map("Yes" -> Map("en-us" ->
        "Yes"), "No" -> Map("en-us" -> "No")))
    details += Detail(silo.siloId, background.entityId, "asthma",
      "radiobuttion", "string", listOfValues = Map("Yes" -> Map("en-us" ->
        "Yes"), "No" -> Map("en-us" -> "No")))
    details += Detail(silo.siloId, background.entityId, "emphysema_copd",
      "radiobuttion", "string", listOfValues = Map("Yes" -> Map("en-us" ->
        "Yes"), "No" -> Map("en-us" -> "No")))
    details += Detail(silo.siloId, background.entityId, "other_lung_disease",
      "radiobuttion", "string", listOfValues = Map("Yes" -> Map("en-us" ->
        "Yes"), "No" -> Map("en-us" -> "No")))
    details += Detail(silo.siloId, background.entityId, "heart_disease",
      "radiobuttion", "string", listOfValues = Map("Yes" -> Map("en-us" ->
        "Yes"), "No" -> Map("en-us" -> "No")))
    details += Detail(silo.siloId, background.entityId, "arthritis",
      "radiobuttion", "string", listOfValues = Map("Yes" -> Map("en-us" ->
        "Yes"), "No" -> Map("en-us" -> "No")))
    details += Detail(silo.siloId, background.entityId, "cancer",
      "radiobuttion", "string", listOfValues = Map("Yes" -> Map("en-us" ->
        "Yes"), "No" -> Map("en-us" -> "No")))
    details += Detail(silo.siloId, background.entityId, "other_chronic",
      "radiobuttion", "string", listOfValues = Map("Yes" -> Map("en-us" ->
        "Yes"), "No" -> Map("en-us" -> "No")))

    details += Detail(silo.siloId, generalHealth.entityId, "health",
      "radiobuttion", "string", listOfValues = Map("Excellent" -> Map("en-us"
        -> "Excellent"), "Very good" -> Map("en-us" -> "Very good"), "Good"
        -> Map("en-us" -> "Good"), "Fair" -> Map("en-us" -> "Fair"), "Poor"
        -> Map("en-us" -> "Poor")))
    details += Detail(silo.siloId, generalHealth.entityId,
      "discouraged_by_problems", "radiobuttion", "string", listOfValues = Map
      ("0" -> Map("en-us" -> "None of the time"), "1" -> Map("en-us" -> "A " +
        "little of the time"), "2" -> Map("en-us" -> "Some of the time"), "3"
        -> Map("en-us" -> "A good bit of the time"), "4" -> Map("en-us" ->
        "Most of the time"), "5" -> Map("en-us" -> "All of the time")))
    details += Detail(silo.siloId, generalHealth.entityId,
      "fearful_future_health", "radiobuttion", "string", listOfValues = Map
      ("0" -> Map("en-us" -> "None of the time"), "1" -> Map("en-us" -> "A " +
        "little of the time"), "2" -> Map("en-us" -> "Some of the time"), "3"
        -> Map("en-us" -> "A good bit of the time"), "4" -> Map("en-us" ->
        "Most of the time"), "5" -> Map("en-us" -> "All of the time")))
    details += Detail(silo.siloId, generalHealth.entityId, "health_worry",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" ->
        "None of the time"), "1" -> Map("en-us" -> "A little of the time"),
        "2" -> Map("en-us" -> "Some of the time"), "3" -> Map("en-us" -> "A " +
          "good bit of the time"), "4" -> Map("en-us" -> "Most of the time"),
        "5" -> Map("en-us" -> "All of the time")))
    details += Detail(silo.siloId, generalHealth.entityId,
      "frustrated_health_problems", "radiobuttion", "string", listOfValues =
        Map("0" -> Map("en-us" -> "None of the time"), "1" -> Map("en-us" ->
          "A little of the time"), "2" -> Map("en-us" -> "Some of the time"),
          "3" -> Map("en-us" -> "A good bit of the time"), "4" -> Map("en-us"
            -> "Most of the time"), "5" -> Map("en-us" -> "All of the time")))
    details += Detail(silo.siloId, generalHealth.entityId, "fatigue",
      "text-field", "integer")
    details += Detail(silo.siloId, generalHealth.entityId, "breath",
      "text-field", "integer")
    details += Detail(silo.siloId, generalHealth.entityId, "pain",
      "text-field", "integer")

    details += Detail(silo.siloId, physicalActivities.entityId, "stretch",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" ->
        "None"), "1" -> Map("en-us" -> "Less than 30 min/wk"), "2" -> Map
      ("en-us" -> "30-60 min/wk"), "3" -> Map("en-us" -> "1-3 hrs per week"),
        "4" -> Map("en-us" -> "more than e hrs/wk")))
    details += Detail(silo.siloId, physicalActivities.entityId, "walk",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" ->
        "None"), "1" -> Map("en-us" -> "Less than 30 min/wk"), "2" -> Map
      ("en-us" -> "30-60 min/wk"), "3" -> Map("en-us" -> "1-3 hrs per week"),
        "4" -> Map("en-us" -> "more than e hrs/wk")))
    details += Detail(silo.siloId, physicalActivities.entityId, "swimming",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" ->
        "None"), "1" -> Map("en-us" -> "Less than 30 min/wk"), "2" -> Map
      ("en-us" -> "30-60 min/wk"), "3" -> Map("en-us" -> "1-3 hrs per week"),
        "4" -> Map("en-us" -> "more than e hrs/wk")))
    details += Detail(silo.siloId, physicalActivities.entityId, "bicycling",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" ->
        "None"), "1" -> Map("en-us" -> "Less than 30 min/wk"), "2" -> Map
      ("en-us" -> "30-60 min/wk"), "3" -> Map("en-us" -> "1-3 hrs per week"),
        "4" -> Map("en-us" -> "more than e hrs/wk")))
    details += Detail(silo.siloId, physicalActivities.entityId,
      "aerobic_equipment", "radiobuttion", "string", listOfValues = Map("0"
        -> Map("en-us" -> "None"), "1" -> Map("en-us" -> "Less than 30 " +
        "min/wk"), "2" -> Map("en-us" -> "30-60 min/wk"), "3" -> Map("en-us"
        -> "1-3 hrs per week"), "4" -> Map("en-us" -> "more than e hrs/wk")))
    details += Detail(silo.siloId, physicalActivities.entityId, "aerobic",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" ->
        "None"), "1" -> Map("en-us" -> "Less than 30 min/wk"), "2" -> Map
      ("en-us" -> "30-60 min/wk"), "3" -> Map("en-us" -> "1-3 hrs per week"),
        "4" -> Map("en-us" -> "more than e hrs/wk")))

    details += Detail(silo.siloId, activities.entityId, "keep_fatigue",
      "text-field", "integer")
    details += Detail(silo.siloId, activities.entityId,
      "physical_discomfort", "text-field", "integer")
    details += Detail(silo.siloId, activities.entityId, "emotional_distress",
      "text-field", "integer")
    details += Detail(silo.siloId, activities.entityId,
      "other_problems_interfering", "text-field", "integer")
    details += Detail(silo.siloId, activities.entityId, "reduce_need_doctor",
      "text-field", "integer")
    details += Detail(silo.siloId, activities.entityId,
      "reduce_illness_affects", "text-field", "integer")
    details += Detail(silo.siloId, activities.entityId,
      "normal_social_activities", "radiobuttion", "string", listOfValues =
        Map("0" -> Map("en-us" -> "Not at all"), "1" -> Map("en-us" ->
          "Slightly"), "2" -> Map("en-us" -> "Moderately"), "3" -> Map
        ("en-us" -> "Quite a bit"), "4" -> Map("en-us" -> "Almost totally")))
    details += Detail(silo.siloId, activities.entityId, "hobbies",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" -> "Not" +
        " at all"), "1" -> Map("en-us" -> "Slightly"), "2" -> Map("en-us" ->
        "Moderately"), "3" -> Map("en-us" -> "Quite a bit"), "4" -> Map
      ("en-us" -> "Almost totally")))
    details += Detail(silo.siloId, activities.entityId, "household_chores",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" -> "Not" +
        " at all"), "1" -> Map("en-us" -> "Slightly"), "2" -> Map("en-us" ->
        "Moderately"), "3" -> Map("en-us" -> "Quite a bit"), "4" -> Map
      ("en-us" -> "Almost totally")))
    details += Detail(silo.siloId, activities.entityId, "errands_shopping",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" -> "Not" +
        " at all"), "1" -> Map("en-us" -> "Slightly"), "2" -> Map("en-us" ->
        "Moderately"), "3" -> Map("en-us" -> "Quite a bit"), "4" -> Map
      ("en-us" -> "Almost totally")))

    details += Detail(silo.siloId, medicalCare.entityId, "questions_doctor",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" ->
        "Never"), "1" -> Map("en-us" -> "Almost never"), "2" -> Map("en-us"
        -> "Sometimes"), "3" -> Map("en-us" -> "Fairly often"), "4" -> Map
      ("en-us" -> "Very often"), "5" -> Map("en-us" -> "Always")))
    details += Detail(silo.siloId, medicalCare.entityId,
      "questions_treatment", "radiobuttion", "string", listOfValues = Map("0"
        -> Map("en-us" -> "Never"), "1" -> Map("en-us" -> "Almost never"),
        "2" -> Map("en-us" -> "Sometimes"), "3" -> Map("en-us" -> "Fairly " +
          "often"), "4" -> Map("en-us" -> "Very often"), "5" -> Map("en-us"
          -> "Always")))
    details += Detail(silo.siloId, medicalCare.entityId, "personal_problems",
      "radiobuttion", "string", listOfValues = Map("0" -> Map("en-us" ->
        "Never"), "1" -> Map("en-us" -> "Almost never"), "2" -> Map("en-us"
        -> "Sometimes"), "3" -> Map("en-us" -> "Fairly often"), "4" -> Map
      ("en-us" -> "Very often"), "5" -> Map("en-us" -> "Always")))
    details += Detail(silo.siloId, medicalCare.entityId, "physician_visits",
      "text-field", "integer")
    details += Detail(silo.siloId, medicalCare.entityId,
      "times_hospital_emergency", "text-field", "integer")
    details += Detail(silo.siloId, medicalCare.entityId,
      "times_hospitalized", "text-field", "integer")
    details += Detail(silo.siloId, medicalCare.entityId, "nights_hospital",
      "text-field", "integer")
    details += Detail(silo.siloId, medicalCare.entityId,
      "hospitalizations_nursing_facility", "radiobuttion", "string",
      listOfValues = Map("Yes" -> Map("en-us" -> "Yes"), "No" -> Map("en-us"
        -> "No")))

    sc.parallelize(details)
      .saveToCassandra("notos_metadata", "detail", SomeColumns("silo_id",
        "entity_id", "detail_id", "control_type", "data_type", "detail_type",
        "list_of_values", "part_of_key"))

    println("Inserting containers")
    val patients = for (i <- 0 until numberOfPatients) yield Container(silo
      .siloId, patient.entityId, UUIDs.timeBased(),
      Map(details(0).detailId -> getRandomKeyOfMap(details(0).listOfValues),
        details(1).detailId -> getRandomBirthday()))
    sc.parallelize(patients)
      .saveToCassandra("notos_data", "container", SomeColumns("silo_id",
        "entity_id", "container_id", "x1"))

    val patientBackground: IndexedSeq[(Container, ContainerAssociation)] =
      for (i <- patients)
      yield createPatientBackground(i, UUIDs.timeBased(), details.filter(_
        .entityId == background.entityId))

    sc.parallelize(patientBackground.map(_._1))
      .saveToCassandra("notos_data", "container", SomeColumns("silo_id",
        "entity_id", "container_id", "x1"))

    sc.parallelize(patientBackground.map(_._2))
      .saveToCassandra("notos_data", "container_association", SomeColumns
      ("silo_id", "from_entity_id", "from_container_id",
        "association_type_id", "to_entity_id", "to_container_id",
        "equal_from_to"))

    val patientGeneralHealth: IndexedSeq[(Container, ContainerAssociation)] =
      for (i <- patients)
      yield createPatientGeneralHealth(i, UUIDs.timeBased(), details.filter(_
        .entityId == generalHealth.entityId))

    sc.parallelize(patientGeneralHealth.map(_._1))
      .saveToCassandra("notos_data", "container", SomeColumns("silo_id",
        "entity_id", "container_id", "x1"))

    sc.parallelize(patientGeneralHealth.map(_._2))
      .saveToCassandra("notos_data", "container_association", SomeColumns
      ("silo_id", "from_entity_id", "from_container_id",
        "association_type_id", "to_entity_id", "to_container_id",
        "equal_from_to"))

    val patientPhysicalActivities: IndexedSeq[(Container,
      ContainerAssociation)] = for (i <- patients)
      yield createPatientPhysicalActivities(i, UUIDs.timeBased(), details
        .filter(_.entityId == physicalActivities.entityId))

    sc.parallelize(patientPhysicalActivities.map(_._1))
      .saveToCassandra("notos_data", "container", SomeColumns("silo_id",
        "entity_id", "container_id", "x1"))

    sc.parallelize(patientPhysicalActivities.map(_._2))
      .saveToCassandra("notos_data", "container_association", SomeColumns
      ("silo_id", "from_entity_id", "from_container_id",
        "association_type_id", "to_entity_id", "to_container_id",
        "equal_from_to"))

    val patientActivities: IndexedSeq[(Container, ContainerAssociation)] =
      for (i <- patients)
      yield createPatientActivities(i, UUIDs.timeBased(), details.filter(_
        .entityId == activities.entityId))

    sc.parallelize(patientActivities.map(_._1))
      .saveToCassandra("notos_data", "container", SomeColumns("silo_id",
        "entity_id", "container_id", "x1"))

    sc.parallelize(patientActivities.map(_._2))
      .saveToCassandra("notos_data", "container_association", SomeColumns
      ("silo_id", "from_entity_id", "from_container_id",
        "association_type_id", "to_entity_id", "to_container_id",
        "equal_from_to"))

    val patientMedicalCare: IndexedSeq[(Container, ContainerAssociation)] =
      for (i <- patients)
      yield createPatientMedicalCare(i, UUIDs.timeBased(), details.filter(_
        .entityId == medicalCare.entityId))

    sc.parallelize(patientMedicalCare.map(_._1))
      .saveToCassandra("notos_data", "container", SomeColumns("silo_id",
        "entity_id", "container_id", "x1"))

    sc.parallelize(patientMedicalCare.map(_._2))
      .saveToCassandra("notos_data", "container_association", SomeColumns
      ("silo_id", "from_entity_id", "from_container_id",
        "association_type_id", "to_entity_id", "to_container_id",
        "equal_from_to"))

    println("Inserting queries")
    val queries = new ListBuffer[Query]
    queries += Query(silo.siloId, UUIDs.timeBased(), Tuple2(medicalCare
      .entityId, details.filter(_.entityId == medicalCare.entityId)(3)
      .detailId), null)
    queries += Query(silo.siloId, UUIDs.timeBased(), Tuple2(medicalCare
      .entityId, details.filter(_.entityId == medicalCare.entityId)(3)
      .detailId), Map(Tuple2(patient.entityId, details(0).detailId) -> "= " +
      "'Female'"))

    sc.parallelize(queries)
      .saveToCassandra("notos_analytics", "query", SomeColumns("silo_id",
        "query_id", "select_clause", "where_clause"))
  }

  def createPatientBackground(patient: Container, containerId: UUID, details:
  Seq[Detail]): (Container, ContainerAssociation) = {
    val container = Container(patient.siloId, details(0).entityId, containerId,
      Map(details(0).detailId -> getRandomKeyOfMap(details(0).listOfValues),
        details(1).detailId -> getRandomKeyOfMap(details(1).listOfValues),
        details(2).detailId -> getRandomKeyOfMap(details(2).listOfValues),
        details(3).detailId -> getRandomKeyOfMap(details(3).listOfValues),
        details(4).detailId -> getRandomKeyOfMap(details(4).listOfValues),
        details(5).detailId -> getRandomKeyOfMap(details(5).listOfValues),
        details(6).detailId -> getRandomKeyOfMap(details(6).listOfValues),
        details(7).detailId -> getRandomKeyOfMap(details(7).listOfValues),
        details(8).detailId -> getRandomKeyOfMap(details(8).listOfValues),
        details(9).detailId -> getRandomKeyOfMap(details(9).listOfValues),
        details(10).detailId -> getRandomKeyOfMap(details(10).listOfValues)))

    val association = ContainerAssociation(patient.siloId, patient.entityId,
      patient.containerId, toEntityId = details(0).entityId,
      toContainerId = containerId)
    (container, association)
  }

  def createPatientGeneralHealth(patient: Container, containerId: UUID,
                                 details: Seq[Detail]): (Container,
    ContainerAssociation) = {
    val container = Container(patient.siloId, details(0).entityId, containerId,
      Map(details(0).detailId -> getRandomKeyOfMap(details(0).listOfValues),
        details(1).detailId -> getRandomKeyOfMap(details(1).listOfValues),
        details(2).detailId -> getRandomKeyOfMap(details(2).listOfValues),
        details(3).detailId -> getRandomKeyOfMap(details(3).listOfValues),
        details(4).detailId -> getRandomKeyOfMap(details(4).listOfValues),
        details(5).detailId -> randBetween(0, 10).toString,
        details(6).detailId -> randBetween(0, 10).toString,
        details(7).detailId -> randBetween(0, 10).toString))

    val association = ContainerAssociation(patient.siloId, patient.entityId,
      patient.containerId, toEntityId = details(0).entityId,
      toContainerId = containerId)
    (container, association)
  }

  def createPatientPhysicalActivities(patient: Container, containerId: UUID,
                                      details: Seq[Detail]): (Container,
    ContainerAssociation) = {
    val container = Container(patient.siloId, details(0).entityId, containerId,
      Map(details(0).detailId -> getRandomKeyOfMap(details(0).listOfValues),
        details(1).detailId -> getRandomKeyOfMap(details(1).listOfValues),
        details(2).detailId -> getRandomKeyOfMap(details(2).listOfValues),
        details(3).detailId -> getRandomKeyOfMap(details(3).listOfValues),
        details(4).detailId -> getRandomKeyOfMap(details(4).listOfValues),
        details(5).detailId -> getRandomKeyOfMap(details(5).listOfValues)))

    val association = ContainerAssociation(patient.siloId, patient.entityId,
      patient.containerId, toEntityId = details(0).entityId,
      toContainerId = containerId)
    (container, association)
  }

  def createPatientActivities(patient: Container, containerId: UUID, details:
  Seq[Detail]): (Container, ContainerAssociation) = {
    val container = Container(patient.siloId, details(0).entityId, containerId,
      Map(details(0).detailId -> randBetween(1, 10).toString,
        details(1).detailId -> randBetween(1, 10).toString,
        details(2).detailId -> randBetween(1, 10).toString,
        details(3).detailId -> randBetween(1, 10).toString,
        details(4).detailId -> randBetween(1, 10).toString,
        details(5).detailId -> randBetween(1, 10).toString,
        details(6).detailId -> getRandomKeyOfMap(details(6).listOfValues),
        details(7).detailId -> getRandomKeyOfMap(details(7).listOfValues),
        details(8).detailId -> getRandomKeyOfMap(details(8).listOfValues),
        details(9).detailId -> getRandomKeyOfMap(details(9).listOfValues)))

    val association = ContainerAssociation(patient.siloId, patient.entityId,
      patient.containerId, toEntityId = details(0).entityId,
      toContainerId = containerId)
    (container, association)
  }

  def createPatientMedicalCare(patient: Container, containerId: UUID,
                               details: Seq[Detail]): (Container,
    ContainerAssociation) = {
    val container = Container(patient.siloId, details(0).entityId, containerId,
      Map(details(0).detailId -> getRandomKeyOfMap(details(0).listOfValues),
        details(1).detailId -> getRandomKeyOfMap(details(1).listOfValues),
        details(2).detailId -> getRandomKeyOfMap(details(2).listOfValues),
        details(3).detailId -> randBetween(0, 180).toString,
        details(4).detailId -> randBetween(0, 180).toString,
        details(5).detailId -> randBetween(0, 180).toString,
        details(6).detailId -> randBetween(0, 180).toString,
        details(7).detailId -> getRandomKeyOfMap(details(7).listOfValues)))

    val association = ContainerAssociation(patient.siloId, patient.entityId,
      patient.containerId, toEntityId = details(0).entityId,
      toContainerId = containerId)
    (container, association)
  }

  def getRandomBirthday(): String = {
    val gc = new GregorianCalendar()
    val year = randBetween(1900, 2010)
    gc.set(Calendar.YEAR, year)

    val dayOfYear = randBetween(1, gc.getActualMaximum(Calendar.DAY_OF_YEAR))
    gc.set(Calendar.DAY_OF_YEAR, dayOfYear)

    gc.get(Calendar.YEAR) + "-" + (gc.get(Calendar.MONTH) + 1) + "-" + gc.get
    (Calendar.DAY_OF_MONTH)
  }

  def getRandomKeyOfMap(map: Map[String, Map[String, String]]): String = {
    val r = new Random();
    map.toSeq(r.nextInt(map.size))._1
  }

  def randBetween(start: Int, end: Int): Int = {
    start + math.round(math.random * (end - start)).toInt
  }

  def truncateKeyspace(conf: SparkConf) = {
    CassandraConnector(conf).withSessionDo { session =>
      session.execute("TRUNCATE notos_data.container;")
      session.execute("TRUNCATE notos_data.container_association;")
      session.execute("TRUNCATE notos_metadata.detail;")
      session.execute("TRUNCATE notos_metadata.entity;")
      session.execute("TRUNCATE notos_silo.silo;")
      session.execute("TRUNCATE notos_analytics.query;")
    }
  }
}
