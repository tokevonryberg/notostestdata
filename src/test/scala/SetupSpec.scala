import Setup._
import org.apache.spark.SparkContext
import org.scalatest.FunSpec

/**
  * Created by tokevonryberg on 29/03/16.
  */
class SetupSpec extends FunSpec {
  describe("connect") {
    it("should return a SparkContext") {
      val testSc = connect()
      assert(testSc.isInstanceOf[SparkContext])
      assert(!testSc.isStopped)
      disconnect(testSc)
    }
  }

  describe("disconnect") {
    it("should disconnect a spark context") {
      val testSc = connect()
      disconnect(testSc)
      assert(testSc.isStopped)
    }
  }
}
